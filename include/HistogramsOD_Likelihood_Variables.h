#ifndef HistogramsOD_Likelihood_Variables_H
#define HistogramsOD_Likelihood_Variables_H

#include "EventBase.h"
#include "HistSvc.h"

class HistogramsOD_Likelihood_Variables {

public:
  /// Constructor
  HistogramsOD_Likelihood_Variables(HistSvc *hists);
  /// Destructor
  ~HistogramsOD_Likelihood_Variables();

  /// Fill g_parameter_binning with bin values
  void Set_Parameter_Binning();

  /**
   * @brief Search g_parameter_binning to get the bins associated with a variable
   * @param variable
   * @return
   */
  std::vector<int> Get_Parameter_Binning(std::string variable);

  /**
   * @brief Fill the histogram associated with Event Pulse Multiplicity
   * @param hist_path path to histogram and histogram name
   * @param multiplicity
   */
  void Multiplicity_Histogram(std::string hist_path, int multiplicity);

  /**
   * @brief Create a set of histograms for a single pulse
   * @param base_path path within ROOT file
   * @param pulse_parameters
   * @param event_phd
   */
  void Single_Pulse_Histograms(std::string base_path,
                               std::vector<std::pair<std::string,float>> pulse_parameters,
                               float event_phd);

  /**
   * @brief Fill Histograms comparing two pulses
   * @param base_path path within ROOT file
   * @param pulse1_parameters
   * @param pulse2_parameters
   */
  void Two_Pulse_Comparison_Histograms(std::string base_path,
                                       std::vector<std::pair<std::string,float>> pulse1_parameters,
                                       std::vector<std::pair<std::string,float>> pulse2_parameters);

  /**
   * @brief Fill histogram with event pulse area
   * @param pulseArea_phd
   */
  void Event_Pulse_Area(float pulseArea_phd);

private:
  /// ALPACA histogram functions
  HistSvc *m_hists;

  std::vector<std::pair<std::string, std::vector<int>>> g_parameter_binning;


};

#endif
