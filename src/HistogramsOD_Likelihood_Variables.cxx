#include "HistogramsOD_Likelihood_Variables.h"
#include "HistSvc.h"

HistogramsOD_Likelihood_Variables::HistogramsOD_Likelihood_Variables(
    HistSvc *hists) {
  m_hists = hists;
  Set_Parameter_Binning();
}

HistogramsOD_Likelihood_Variables::~HistogramsOD_Likelihood_Variables() {}

void HistogramsOD_Likelihood_Variables::Multiplicity_Histogram(
    std::string hist_path, int multiplicity) {
  m_hists->BookFillHist(hist_path, 1500., 0., 1500., multiplicity);
  return;
}

void HistogramsOD_Likelihood_Variables::Set_Parameter_Binning() {

  // TODO Use std::map as O(log n), std::pair is O(n)

  std::vector<int> pulseArea_phd_bins = {1500, 0, 1500};
  g_parameter_binning.push_back(std::make_pair("pulseArea_phd", pulseArea_phd_bins ));
  std::vector<int> coincidence_bins = {120, 0, 120};
  g_parameter_binning.push_back(std::make_pair("coincidence", coincidence_bins ));
  std::vector<int> peakAmp_bins = {100, 0, 14};
  g_parameter_binning.push_back(std::make_pair("peakAmp", peakAmp_bins ));
  std::vector<int> time_bins = {150, 0, 1500};
  g_parameter_binning.push_back(std::make_pair("peakTime_ns", time_bins ));
  g_parameter_binning.push_back(std::make_pair("areaFractionTime5_ns", time_bins ));
  g_parameter_binning.push_back(std::make_pair("areaFractionTime10_ns", time_bins ));
  g_parameter_binning.push_back(std::make_pair("areaFractionTime25_ns", time_bins ));
  g_parameter_binning.push_back(std::make_pair("areaFractionTime50_ns", time_bins ));
  g_parameter_binning.push_back(std::make_pair("areaFractionTime75_ns", time_bins ));
  g_parameter_binning.push_back(std::make_pair("areaFractionTime90_ns", time_bins ));
  g_parameter_binning.push_back(std::make_pair("areaFractionTime95_ns-areaFractionTime5_ns", time_bins ));
  g_parameter_binning.push_back(std::make_pair("areaFractionTime99_ns-areaFractionTime1_ns", time_bins ));
  g_parameter_binning.push_back(std::make_pair("rmsWidth_ns", time_bins ));
  g_parameter_binning.push_back(std::make_pair("pulseStartTime_ns", time_bins ));
  g_parameter_binning.push_back(std::make_pair("pulseEndTime_ns-pulseStartTime_ns", time_bins ));
}

std::vector<int> HistogramsOD_Likelihood_Variables::Get_Parameter_Binning(std::string variable) {

  for (int i=0; i < g_parameter_binning.size(); ++i) {
    if (g_parameter_binning[i].first == variable) {
      return g_parameter_binning[i].second;
    }
  }
  FATAL("Did not find '" + variable + "' in g_parameter_binning");
  exit(0);
}

void HistogramsOD_Likelihood_Variables::Single_Pulse_Histograms(
    std::string base_path, std::vector<std::pair<std::string, float>> pulse_parameters,
    float event_phd) {

  std::vector<int> i_variable_bins;
  std::vector<int> j_variable_bins;

  // Loop over pulse parameters to make 1D and 2D histograms
  for (int i=0; i < pulse_parameters.size(); ++i) {
    // Load variable binning
    i_variable_bins = Get_Parameter_Binning(pulse_parameters[i].first);

    for (int j=0; j < pulse_parameters.size(); ++j) {

      // x = y so plot 1D histogram
     if (i == j) {
       m_hists->BookFillHist(base_path + "/RQs/" + pulse_parameters[i].first,
                             i_variable_bins[0], i_variable_bins[1], i_variable_bins[2],
                             pulse_parameters[i].second);
     }
     else {
       // Load variable binning
       j_variable_bins = Get_Parameter_Binning(pulse_parameters[j].first);

       // 2D
       m_hists->BookFillHist(base_path + "/VS/2D/" +
                             pulse_parameters[i].first + "_vs_" +
                             pulse_parameters[j].first,
                             i_variable_bins[0], i_variable_bins[1], i_variable_bins[2],
                             j_variable_bins[0], j_variable_bins[1], j_variable_bins[2],
                             pulse_parameters[i].second, pulse_parameters[j].second);

       // 1D
       m_hists->BookFillHist(base_path + "/VS/1D/" +
                             pulse_parameters[i].first + "_vs_" +
                             pulse_parameters[j].first,
                             100, 0., i_variable_bins[2]/j_variable_bins[2],
                             pulse_parameters[i].second / pulse_parameters[j].second);
     }

    }

    if (pulse_parameters[i].first == "pulseArea_phd") {
      // Fraction of Event
      m_hists->BookFillHist(base_path + "/FractionOfEvent/pulseArea_phd_fraction",
                            100., 0., 1., pulse_parameters[i].second / event_phd);
      m_hists->BookFillHist(base_path + "/FractionOfEvent/pulseArea_phd_2D",
                            i_variable_bins[0], i_variable_bins[1], i_variable_bins[2],
                            i_variable_bins[0], i_variable_bins[1], i_variable_bins[2],
                            pulse_parameters[i].second, event_phd);
      m_hists->BookFillHist(base_path + "/FractionOfEvent/pulseArea_phd_2D_",
                            i_variable_bins[0], i_variable_bins[1], i_variable_bins[2],
                            i_variable_bins[0], i_variable_bins[1], i_variable_bins[2],
                            pulse_parameters[i].second,
                            event_phd - pulse_parameters[i].second);
    }
  }

  return;
}

void HistogramsOD_Likelihood_Variables::Two_Pulse_Comparison_Histograms(
    std::string base_path, std::vector<std::pair<std::string, float>> pulse1_parameters,
    std::vector<std::pair<std::string, float>> pulse2_parameters) {
  std::vector<int> i_variable_bins;
  for (int i=0; i < pulse1_parameters.size(); ++i) {
    if (pulse1_parameters[i].first == "pulseArea_phd") {
      i_variable_bins = Get_Parameter_Binning("pulseArea_phd");
      m_hists->BookFillHist(base_path + "/2D/pulseArea_phd", i_variable_bins[0],
                            i_variable_bins[1], i_variable_bins[2],
                            i_variable_bins[0], i_variable_bins[1],
                            i_variable_bins[2], pulse1_parameters[i].second,
                            pulse2_parameters[i].second);
    }
    else if (pulse1_parameters[i].first == "pulseStartTime_ns") {
      m_hists->BookFillHist(base_path + "/TimeDifference/pulseStartTime_ns", 1000.,
                            -5000., 5000.,
                            pulse1_parameters[i].second - pulse2_parameters[i].second);

    }


  }
  return;
}

void HistogramsOD_Likelihood_Variables::Event_Pulse_Area(float pulseArea_phd)
{
  m_hists->BookFillHist("Event_pulseArea_phd", 1500., 0., 1500., pulseArea_phd);
  return;

}

