#include "OD_Likelihood_Variables.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsOD_Likelihood_Variables.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "HistogramsOD_Likelihood_Variables.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

OD_Likelihood_Variables::OD_Likelihood_Variables() : Analysis() {
  // Setup config service so it can be used to get config variables
  m_conf = ConfigSvc::Instance();
  g_od_gain_channel = m_conf->configFileVarMapString["OD_Gain"];
  INFO("Using OD pulses from " + g_od_gain_channel + " Gain Channel");

  // Load ROOT branches
  m_event->IncludeBranch("eventHeader");
  // only load either low gain or high gain, not both as not needed
  if (g_od_gain_channel == "High") {
    m_event->IncludeBranch("pulsesODHG");
  } else if (g_od_gain_channel == "Low") {
    m_event->IncludeBranch("pulsesODLG");
  } else {
    FATAL("OD Gain requested is not valid");
    exit(0);
  }
  m_event->Initialize();

  // Setup logging
  logging::set_program_name("OD_Likelihood_Variables Analysis");

  // Setup the analysis specific cuts.
  m_cutsOD_Likelihood_Variables =
      new CutsOD_Likelihood_Variables(m_event, m_conf);

  // Setup the histograms
  m_histograms = new HistogramsOD_Likelihood_Variables(m_hists);
}

OD_Likelihood_Variables::~OD_Likelihood_Variables() {
  delete m_cutsOD_Likelihood_Variables;
  delete m_histograms;
}

void OD_Likelihood_Variables::Initialize() {
  INFO("Initializing OD_Likelihood_Variables Analysis");
}

void OD_Likelihood_Variables::Execute() {
  // Select the branch and begin the processing
  if (g_od_gain_channel == "High") {
    Process_Event((m_event->m_odHGPulses));
  } else if (g_od_gain_channel == "Low") {
    Process_Event((m_event->m_odLGPulses));
  }
}

void OD_Likelihood_Variables::Finalize() {
  INFO("Finalizing OD_Likelihood_Variables Analysis");
}

void OD_Likelihood_Variables::Process_Event(
    TTreeReaderValue<ODPulses> *event_pulses) {

  if (!m_cutsOD_Likelihood_Variables->Check_Event_ID())
    return;

  // Get the pulse IDs
  std::vector<int> pulse_ids_all{Get_OD_Pulse_IDs(event_pulses)};
  m_histograms->Multiplicity_Histogram("Event_Multiplicity_w0",
                                       pulse_ids_all.size());
  // Do no more if there are no pulses
  if (pulse_ids_all.size() < 1)
    return;
  g_OD_event_phd = Calculate_Pulse_Area(event_pulses, pulse_ids_all);
  m_histograms->Multiplicity_Histogram("Event_Multiplicity",
                                       pulse_ids_all.size());

  // Cut Events
  std::vector<int> pulse_ids_cut{Cut_Pulse_IDs(event_pulses, pulse_ids_all)};
  m_histograms->Multiplicity_Histogram("CutPulses_Multiplicity_w0",
                                       pulse_ids_cut.size());
  // Do no more if there are no pulses
  if (pulse_ids_cut.size() < 1)
    return;
  m_histograms->Multiplicity_Histogram("CutPulses_Multiplicity",
                                       pulse_ids_cut.size());

  // Now create variables
  g_OD_event_phd = Calculate_Pulse_Area(event_pulses, pulse_ids_all);
  g_OD_cut_event_phd = Calculate_Pulse_Area(event_pulses, pulse_ids_cut);

  m_histograms->Event_Pulse_Area(g_OD_event_phd);
  // std::vector<int> pulse_ids_ordered_all{Order_Pulses_By_Phd(event_pulses,
  // pulse_ids_all)};
  std::vector<int> pulse_ids_ordered_cut{
      Order_Pulses_By_Phd(event_pulses, pulse_ids_cut)};

  // Look at the largest pulse
  std::vector<std::pair<std::string, float>>  pulse_parameters_first{
      Get_Single_Pulse_Parameters_OD(event_pulses, pulse_ids_ordered_cut[0])};
  std::string base_path{
      "CutPulses_phd" +
      std::to_string(m_conf->configFileVarMap["Pulse_Area"]) +
      "_coincidence" +
      std::to_string(
          m_conf->configFileVarMap["Pulse_Coincidence"])};
  m_histograms->Single_Pulse_Histograms(
      base_path + "/First", pulse_parameters_first, g_OD_cut_event_phd);

  // Stop if no more pulses
  if (pulse_ids_cut.size() < 2)
    return;

  // Look for second largest pulse
  std::vector<std::pair<std::string, float>>  pulse_parameters_second{
      Get_Single_Pulse_Parameters_OD(event_pulses, pulse_ids_ordered_cut[1])};
  m_histograms->Single_Pulse_Histograms(
      base_path + "/Second", pulse_parameters_second, g_OD_cut_event_phd);
  m_histograms->Two_Pulse_Comparison_Histograms(base_path + "/First_and_Second",
                                                pulse_parameters_first,
                                                pulse_parameters_second);
}

std::vector<int> OD_Likelihood_Variables::Get_OD_Pulse_IDs(
    TTreeReaderValue<ODPulses> *event_pulses) {
  std::vector<int> pulse_ids;
  for (int i = 0; i < (*event_pulses)->nPulses; ++i) {
    pulse_ids.push_back(i);
  }
  return pulse_ids;
}

float OD_Likelihood_Variables::Calculate_Pulse_Area(
    TTreeReaderValue<ODPulses> *event_pulses, std::vector<int> pulse_ids) {
  float phd{0.};
  for (auto &&id : pulse_ids) {
    phd += (*event_pulses)->pulseArea_phd[id];
  }
  return phd;
}

std::vector<int> OD_Likelihood_Variables::Order_Pulses_By_Phd(
    TTreeReaderValue<ODPulses> *event_pulses, std::vector<int> pulse_ids) {
  std::vector<std::pair<int, float>> pulses_pair; // pulse id and pulse phd
  std::vector<int> sorted_pulses;
  // Make pair for sorting
  for (auto &&id : pulse_ids) {
    pulses_pair.push_back(
        std::make_pair(id, (*event_pulses)->pulseArea_phd[id]));
  }
  auto sort_by_phd = [](const std::pair<int, float> &lhs,
                        const std::pair<int, float> &rhs) {
    return lhs.second > rhs.second;
  };
  std::sort(pulses_pair.begin(), pulses_pair.end(), sort_by_phd);
  // Put back into vector
  for (int i = 0; i < pulse_ids.size(); ++i) {
    sorted_pulses.push_back(pulses_pair[i].first);
  }
  return sorted_pulses;
}

std::vector<int>
OD_Likelihood_Variables::Cut_Pulse_IDs(TTreeReaderValue<ODPulses> *event_pulses,
                                       std::vector<int> pulse_ids) {
  std::vector<int> pulse_ids_selected;
  int coincidence;
  float pulseArea_phd;
  for (auto &&id : pulse_ids) {
    coincidence = (*event_pulses)->coincidence[id];
    pulseArea_phd = (*event_pulses)->pulseArea_phd[id];
    if (m_cutsOD_Likelihood_Variables->Cut_Pulse_IDs_Phd_Coincidence(
            coincidence, pulseArea_phd)) {
      pulse_ids_selected.push_back(id);
    }
  }
  return pulse_ids_selected;
}


std::vector<std::pair<std::string, float>> OD_Likelihood_Variables::Get_Single_Pulse_Parameters_OD(
    TTreeReaderValue<ODPulses> *event_pulses, int pulse_id) {

  std::vector<std::pair<std::string, float>> pulse_parameters;

  pulse_parameters.push_back(std::make_pair("pulseArea_phd", (*event_pulses)->pulseArea_phd[pulse_id]));
  pulse_parameters.push_back(std::make_pair("coincidence", (*event_pulses)->coincidence[pulse_id]));
  pulse_parameters.push_back(std::make_pair("peakAmp", (*event_pulses)->peakAmp[pulse_id]));
  pulse_parameters.push_back(std::make_pair("peakTime_ns", (*event_pulses)->peakTime_ns[pulse_id]));
  pulse_parameters.push_back(std::make_pair("areaFractionTime10_ns", (*event_pulses)->areaFractionTime10_ns[pulse_id]));
  pulse_parameters.push_back(std::make_pair("areaFractionTime25_ns", (*event_pulses)->areaFractionTime25_ns[pulse_id]));
  pulse_parameters.push_back(std::make_pair("areaFractionTime50_ns", (*event_pulses)->areaFractionTime50_ns[pulse_id]));
  pulse_parameters.push_back(std::make_pair("areaFractionTime75_ns", (*event_pulses)->areaFractionTime75_ns[pulse_id]));
  pulse_parameters.push_back(std::make_pair("areaFractionTime90_ns", (*event_pulses)->areaFractionTime90_ns[pulse_id]));
  pulse_parameters.push_back(std::make_pair("areaFractionTime95_ns-areaFractionTime5_ns", (*event_pulses)->areaFractionTime95_ns[pulse_id]-(*event_pulses)->areaFractionTime5_ns[pulse_id]));
  pulse_parameters.push_back(std::make_pair("areaFractionTime99_ns-areaFractionTime1_ns", (*event_pulses)->areaFractionTime99_ns[pulse_id]-(*event_pulses)->areaFractionTime1_ns[pulse_id]));
  pulse_parameters.push_back(std::make_pair("rmsWidth_ns", (*event_pulses)->rmsWidth_ns[pulse_id]));
  pulse_parameters.push_back(std::make_pair("pulseStartTime_ns", (*event_pulses)->pulseStartTime_ns[pulse_id]));
  pulse_parameters.push_back(std::make_pair("pulseEndTime_ns-pulseStartTime_ns", (*event_pulses)->pulseEndTime_ns[pulse_id] - (*event_pulses)->pulseStartTime_ns[pulse_id]));

  return pulse_parameters;
}